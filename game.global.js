var THREE = THREE || {};



// scene size
var sceneWidth = 800,
sceneHeight = 600,
// scene aspect ratio
aspect = 800.0 / 600.0,
// camera field of view in degrees
fovAngle = 45,
// camera Z-axis clipping planes
nearPlane = 0.1,
farPlane = 10000.0;

// grab the container to place the renderer into
var gameContainer = document.getElementById('game');

// create a scene
var scene = new THREE.Scene();

THREE.Scene.prototype.clear = function() {
    this.children.length = 0;
    return this;
};

// create a camera
var camera = new THREE.PerspectiveCamera(fovAngle, aspect, nearPlane, farPlane);

// create a renderer
var renderer = new THREE.WebGLRenderer();

// move the camera back a bit from origin (0,0,0)
camera.position.z = 15;

// add the camera to the scene
scene.add(camera);

// set the renderer size to match our desired scene size
renderer.setSize(sceneWidth, sceneHeight);

// add the renderer to the container
gameContainer.appendChild(renderer.domElement);

// wrap up scene building inside a function
function build_scene () {
    
}

// build our scene
 // create lights
var ambientLight = new THREE.AmbientLight(0x101010);
scene.add(ambientLight);

var frontLight = new THREE.DirectionalLight('white', 1);
frontLight.position.set(0.5, 0.5, 2.0);
scene.add(frontLight);

var options = {
    font: 'droid serif',
    weight: 'bold',
    size: 1,
    height: 0.4
};

var geometry = new THREE.TextGeometry('Brony Porn Convention', options);
geometry.computeBoundingBox();

var center = new THREE.Vector3();
center.x = (geometry.boundingBox.max.x - geometry.boundingBox.min.x) / 2;
center.z = (geometry.boundingBox.max.z - geometry.boundingBox.min.z) / 2;

geometry.vertices.forEach(function (vtx) {
    vtx.sub(center);
});

var material = new THREE.MeshPhongMaterial({
    color: 0xff0000,
    shininess: 1000,
    specular: 0x808080
});

var mesh = new THREE.Mesh(geometry, material);

scene.add(mesh);

// a basic animation loop
function animate () {
    window.requestAnimationFrame(animate);
    renderer.render(scene, camera);
}

// make it so we can see something
//renderer.render(scene, camera);

// real-time ~60fps animation loop
animate();
